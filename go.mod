module gitlab.com/amedvedev_eyeconweb/notificator

go 1.15

require (
	github.com/go-telegram-bot-api/telegram-bot-api/v5 v5.5.1
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/mail.v2 v2.3.1
)
