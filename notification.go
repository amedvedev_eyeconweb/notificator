package notify

import (
	"crypto/tls"
	"errors"
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	gomail "gopkg.in/mail.v2"
)

const (
	TypeEmail    = "email"
	TypeTelegram = "telegram"
)

type Button struct {
	Name string
	Link string
}

type BaseNotification struct {
	title   string
	content string
	ch      []string
	button  Button
}

func (n *BaseNotification) Button() Button {
	return n.button
}

func (n *BaseNotification) SetButton(name string, link string) Notification {
	n.button.Link = link
	n.button.Name = name
	return n
}

func (n *BaseNotification) Title() string {
	return n.title
}

func (n *BaseNotification) SetContent(c string) Notification {
	n.content = c
	return n
}
func (n *BaseNotification) Content() string {
	return n.content
}

func (n *BaseNotification) Channels() []string {
	return n.ch
}

func NewNotification(title string, ch []string) Notification {
	return &BaseNotification{
		title: title,
		ch:    ch,
	}
}

type Notification interface {
	SetContent(content string) Notification
	Content() string
	Channels() []string
	Title() string
	SetButton(name string, link string) Notification
	Button() Button
}

type Recipient interface {
	EmailAddress() string
	TelegramChatID() int64
}
type Channel interface {
	Notify(n Notification, r Recipient) error
}

type notifier struct {
	channels map[string]Channel
}

func (nr *notifier) Channel(ch string) (Channel, error) {
	if channel, ok := nr.channels[ch]; ok {
		return channel, nil
	}
	return nil, errors.New("channel not found")
}

func (nr *notifier) Send(n Notification, r Recipient) error {
	for _, ch := range n.Channels() {
		channel, err := nr.Channel(ch)
		if err != nil {
			return err
		}
		err = channel.Notify(n, r)
		if err != nil {
			return err
		}
	}
	return nil
}

type TelegramChannel struct {
	bot *tgbotapi.BotAPI
}

func (e *TelegramChannel) Notify(n Notification, r Recipient) error {
	content := n.Content()
	if n.Button().Name != "" {
		content = fmt.Sprintf("%s, %s: %s", content, n.Button().Name, n.Button().Link)
	}
	msg := tgbotapi.NewMessage(r.TelegramChatID(), content)
	if _, err := e.bot.Send(msg); err != nil {
		return err
	}
	return nil
}

type EmailChannel struct {
	server   string
	port     int
	username string
	password string
	tls      bool
	sender   string
}

func (e *EmailChannel) Notify(n Notification, r Recipient) error {
	m := gomail.NewMessage()
	// Set E-Mail sender
	m.SetHeader("From", e.sender)
	// Set E-Mail receivers
	m.SetHeader("To", r.EmailAddress())
	// Set E-Mail subject
	m.SetHeader("Subject", n.Title())

	// Set E-Mail body. You can set plain text or html with text/html
	content := n.Content()
	if n.Button().Name != "" {
		content = fmt.Sprintf("%s <a href='%s'>%s</a>", content, n.Button().Link, n.Button().Name)
	}
	m.SetBody("text/html", content)

	// Settings for SMTP server
	d := gomail.NewDialer(e.server, e.port, e.username, e.password)

	// This is only needed when SSL/TLS certificate is not valid on server.
	// In production this should be set to false.
	if e.tls {
		d.TLSConfig = &tls.Config{InsecureSkipVerify: true}
	}

	// Now send E-Mail
	if err := d.DialAndSend(m); err != nil {
		return err
	}
	return nil
}

type Config struct {
	Mail struct {
		Server   string
		Port     int
		Username string
		Password string
		Tls      bool
		Sender   string
	}
	Telegram struct {
		Token string
	}
}

func NewNotifier(c Config) Notifier {
	ch := make(map[string]Channel)

	ch[TypeEmail] = &EmailChannel{
		c.Mail.Server,
		c.Mail.Port,
		c.Mail.Username,
		c.Mail.Password,
		c.Mail.Tls,
		c.Mail.Sender,
	}

	bot, err := tgbotapi.NewBotAPI(c.Telegram.Token)
	bot.Debug = true
	if err != nil {
		panic(err)
	}
	tch := &TelegramChannel{
		bot: bot,
	}
	ch[TypeTelegram] = tch

	return &notifier{
		channels: ch,
	}
}

type Notifier interface {
	Send(n Notification, r Recipient) error
}
